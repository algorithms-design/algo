/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apassara.algolongtime;

/**
 *
 * @author ASUS
 */
public class A1_10 {
    public static void longSortedSubarray(int ar[]) {
        
        int max = 1, len = 1, maxIndex = 0;

        for (int i = 1; i < ar.length; i++) {

            if (ar[i] > ar[i - 1]) {
                len++;
            } else {

                if (max < len) {
                    max = len;

                    maxIndex = i - max;
                }
 
                len = 1;
            }
        }

        if (max < len) {
            max = len;
            maxIndex = ar.length - max;
        }

        for (int i = maxIndex; i < max + maxIndex; i++) {
            System.out.print(ar[i] + " ");
        }
    }
    
    public static void main(String[] args) {
        int ar[] = {3, 5, 3, 8, 9, 1, 2, 7, 4, 5, 6, 7};
        longSortedSubarray(ar);
    }

}
