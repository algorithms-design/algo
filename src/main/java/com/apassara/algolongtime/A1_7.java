/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apassara.algolongtime;

import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author ASUS
 */
public class A1_7 {
    
    public static boolean CheckPair(int A[],int c){
        //c = A[i] + A[j], for 1 ≤ i<j ≤ n.
        // i >= 1 , j > i,n >= j 
        // sum of A[i] + A[j] = c
        for(int i = 0;i < A.length - 1; i++){
            for(int j = i + 1; j < A.length; j++){
                if(A[i] + A[j] == c){
                    //print pair sum of A[i] + A[j] = c
                    System.out.println("A pair that has a sum equal to " + c + " is " + A[i] + " " + A[j]);
                    // return yes (is A pair that has a sum equal to c.)
                    return true;
                }
            }
        }
        //return no (There is no sum equal to c.)
        return false;
    }
    

    public static void main(String[] args) throws IOException{
        Scanner kb = new Scanner(System.in);
        // c --> input
        int c = kb.nextInt();
        int A[] = new int[c];
        
        for(int i = 0; i < c; i++){
            // A[i] --> number in Array that input
            A[i] = kb.nextInt();
        }
        
        //CheckPair ---> Checks pair if any sum is equal to c.
        if(CheckPair(A,c)){
            //If there is pair that gets the sum println Yes
            System.out.println("Yes");
        }else{
            //If there are no pairs that gets the sum println No
            System.out.println("No");
        }

    }

}

