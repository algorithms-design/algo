/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apassara.algolongtime;

import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author ASUS
 */
public class A1_9 {

    public static void main(String[] args) throws IOException{
        Scanner kb = new Scanner(System.in);
        String s = kb.next();
        int n = 0;
        for (int i = 0; i < s.length(); i++) {
            n = Character.getNumericValue(s.charAt(i)); //Use getNumericValue convert string variables to integer
            System.out.print(n);
        }
    }
}
