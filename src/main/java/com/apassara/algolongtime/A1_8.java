/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apassara.algolongtime;

/**
 *
 * @author ASUS
 */
public class A1_8 {
    public static int[] reverseArr(int[] ar) {

        for (int i = 0; i < ar.length / 2; i++) {
            int temp = ar[ar.length - 1 - i];
            ar[ar.length - 1 - i] = ar[i];
            ar[i] = temp;
        }
        for (int i = 0; i < ar.length; i++) {
            System.out.print(ar[i] + " ");
        }
        return ar;
    }

    public static void main(String[] args) {
        int ar[] = {9,8,7,6,5,4,3,2,1};
        reverseArr(ar);
    }

}
